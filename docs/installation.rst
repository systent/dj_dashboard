============
Installation
============

At the command line::

    $ easy_install dj_dashboard

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv dj_dashboard
    $ pip install dj_dashboard

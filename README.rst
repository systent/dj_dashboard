=============================
dj_dashboard
=============================

.. image:: https://badge.fury.io/py/dj_dashboard.png
    :target: https://badge.fury.io/py/dj_dashboard

.. image:: https://travis-ci.org/yourname/dj_dashboard.png?branch=master
    :target: https://travis-ci.org/yourname/dj_dashboard

Django Dashboard

Documentation
-------------

The full documentation is at https://dj_dashboard.readthedocs.org.

Quickstart
----------

Install dj_dashboard::

    pip install dj_dashboard

Then use it in a project::

    import dj_dashboard

Features
--------

* TODO

Running Tests
--------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install -r requirements_test.txt
    (myenv) $ python runtests.py

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage

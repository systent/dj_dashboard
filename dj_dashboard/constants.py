# -*- coding: utf-8 -*


# Contains: GROUP_ID : [GROUP_NAME, GROUP_CSS]
CONSTANTS_WIDGETGROUPS = {
    # 'grptab': [u'Tables', 'tables'],  # Example line
}

# Contains: CLASS_ID : [CLASS_NAME, CLASS_CSS]
CONSTANTS_WIDGETCLASSES = {
    # 'grporganisation': [u'Organisation', 'organisation'],  # Example line
}

# Contains: reverse-name : [TITLE, WIDGET_CLASS, GROUP_NAME]
CONSTANTS_WIDGETS = {
    # 'dashboard-organisation-table': ['Übersicht Organisation', 'grporganisation', 'grptab'], # Example line
}

# Example line for definition in settings.pyr
# DJ_DASH_WIDGETGROUPS = CONSTANTS_WIDGETSGROUPS
# DJ_DASH_WIDGETS = CONSTANTS_WIDGETS
